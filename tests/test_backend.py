import unittest
import numpy
from penne import backend

def generate(shape, dtype=numpy.float):
    na = numpy.random.normal(0., 1., shape).astype(dtype)
    ga = backend.array(na)
    return na, ga

class CreationTestCase(unittest.TestCase):
    def test_fill(self):
        for dtype in [numpy.float32, numpy.int32]:
            na = numpy.empty((2,3), dtype=dtype)
            ga = backend.array(na)
            na.fill(0.5)
            ga.fill(0.5)
            self.assertTrue(numpy.allclose(na, ga))

class ShapeTestCase(unittest.TestCase):
    def setUp(self):
        self.na = numpy.random.uniform(0., 10., (2,3))
        self.ga = backend.array(self.na)
        self.nb = numpy.random.uniform(0., 10., (2,3))
        self.gb = backend.array(self.nb)

    def test_stack(self):
        na, nb = self.na, self.nb
        ga, gb = self.ga, self.gb

        # NumPy 1.9 doesn't have stack yet
        def numpy_stack(arrays, axis):
            arrays = [numpy.expand_dims(a, axis) for a in arrays]
            return numpy.concatenate(arrays, axis)

        for axis in [0, 1, 2]:
            self.assertTrue(numpy.allclose(numpy_stack([na, nb], axis=axis), backend.stack([ga, gb], axis=axis)))

class UnaryTestCase(unittest.TestCase):
    def test_neg(self):
        for dtype1 in [numpy.float32, numpy.int32]:
            na = numpy.random.uniform(0., 10., (2,3)).astype(dtype1)
            ga = backend.array(na)

            self.assertTrue(numpy.allclose(-na, -ga))

            for dtype2 in [numpy.float32, numpy.int32]:
                nb = numpy.empty_like(na, dtype=dtype2)
                gb = backend.empty_like(ga, dtype=dtype2)

                self.assertTrue(numpy.allclose(numpy.negative(na, out=nb), 
                                               backend.negative(ga, out=gb)))

    def test_abs(self):
        for dtype1 in [numpy.float32, numpy.int32]:
            na = numpy.random.uniform(0., 10., (2,3)).astype(dtype1)
            ga = backend.array(na)

            self.assertTrue(numpy.allclose(abs(na), abs(ga)))

            for dtype2 in [numpy.float32, numpy.int32]:
                nb = numpy.empty_like(na, dtype=dtype2)
                gb = backend.empty_like(ga, dtype=dtype2)

                self.assertTrue(numpy.allclose(numpy.absolute(na, out=nb), 
                                               backend.absolute(ga, out=gb)))


class BinaryTestCase(unittest.TestCase):
    def test_add(self):
        for dtype1 in [numpy.float32, numpy.int32]:
            for dtype2 in [numpy.float32, numpy.int32]:
                na = numpy.random.uniform(0., 10., (2,3)).astype(dtype1)
                nb = numpy.random.uniform(0., 10., (2,3)).astype(dtype2)
                ga = backend.array(na)
                gb = backend.array(nb)

                self.assertTrue(numpy.allclose(na+nb, ga+gb))
                na += nb
                ga += gb
                self.assertTrue(numpy.allclose(na, ga))

                for dtype3 in [numpy.float32, numpy.int32]:
                    nc = numpy.empty_like(na, dtype=dtype3)
                    gc = backend.empty_like(ga, dtype=dtype3)

                    self.assertTrue(numpy.allclose(numpy.add(na, nb, out=nc),
                                                   backend.add(ga, gb, out=gc)))

    def test_divide(self):
        for dtype1 in [numpy.float32, numpy.int32]:
            for dtype2 in [numpy.float32, numpy.int32]:
                na = numpy.random.uniform(1., 10., (2,3)).astype(dtype1)
                nb = numpy.random.uniform(1., 10., (2,3)).astype(dtype2)
                ga = backend.array(na)
                gb = backend.array(nb)

                self.assertTrue(numpy.allclose(na/nb, ga/gb))

    def test_power(self):
        for dtype2 in [numpy.float32, numpy.int32]:
            na = numpy.random.uniform(1., 10., (2,3)).astype(numpy.float32)
            nb = numpy.random.uniform(1., 10., (2,3)).astype(dtype2)
            ga = backend.array(na)
            gb = backend.array(nb)

            self.assertTrue(numpy.allclose(na ** nb, ga ** gb))

class LogicalTestCase(unittest.TestCase):
    def test_compare(self):
        na = numpy.random.uniform(0., 10., (2,3)).astype(numpy.float32)
        nb = numpy.random.uniform(0., 10., (2,3)).astype(numpy.float32)
        ga = backend.array(na)
        gb = backend.array(nb)

        self.assertTrue(numpy.allclose(na < nb, ga < gb))
        self.assertTrue(numpy.allclose(na <= nb, ga <= gb))
        self.assertTrue(numpy.allclose(na > nb, ga > gb))
        self.assertTrue(numpy.allclose(na >= nb, ga >= gb))

    def test_operators(self):
        na = numpy.random.randint(2, size=(2,3)).astype(bool)
        nb = numpy.random.randint(2, size=(2,3)).astype(bool)
        ga = backend.array(na)
        gb = backend.array(nb)
        self.assertTrue(numpy.allclose(numpy.logical_not(na), backend.logical_not(ga)))
        self.assertTrue(numpy.allclose(numpy.logical_and(na, nb), backend.logical_and(ga, gb)))
        self.assertTrue(numpy.allclose(numpy.logical_or(na, nb), backend.logical_or(ga, gb)))

        nx = numpy.random.uniform(0., 10., (2,3)).astype(numpy.float32)
        ny = numpy.random.uniform(0., 10., (2,3)).astype(numpy.float32)
        gx = backend.array(nx)
        gy = backend.array(ny)

        self.assertTrue(numpy.allclose(numpy.where(na, nx, ny), backend.where(ga, gx, gy)))
        
class ReductionTestCase(unittest.TestCase):
    def test_asum(self):
        for dtype in [numpy.float32]: # numpy.int32
            na = numpy.random.uniform(0., 10., (2,3,4)).astype(dtype)
            ga = backend.array(na)

            self.assertTrue(numpy.allclose(numpy.sum(na), backend.asum(ga)))
            self.assertTrue(numpy.allclose(numpy.sum(na, axis=0), backend.asum(ga, axis=0)))
            self.assertTrue(numpy.allclose(numpy.sum(na, axis=(0,2)), backend.asum(ga, axis=(0,2))))
            self.assertTrue(numpy.allclose(numpy.sum(na, axis=0, keepdims=True), backend.asum(ga, axis=0, keepdims=True)))
            nb = numpy.empty((1,3,4), dtype=dtype)
            gb = backend.empty((1,3,4), dtype=dtype)
            self.assertTrue(numpy.allclose(numpy.sum(na, axis=0, keepdims=True, out=nb), backend.asum(ga, axis=0, keepdims=True, out=gb)))

    def test_amax(self):
        for dtype in [numpy.float32]: # numpy.int32
            na, ga = generate((2,3,4), dtype)

            self.assertTrue(numpy.allclose(numpy.amax(na), backend.amax(ga)))
            self.assertTrue(numpy.allclose(numpy.amax(na, axis=0), backend.amax(ga, axis=0)))
            self.assertTrue(numpy.allclose(numpy.amax(na, axis=(0,2)), backend.amax(ga, axis=(0,2))))
            self.assertTrue(numpy.allclose(numpy.amax(na, axis=0, keepdims=True), backend.amax(ga, axis=0, keepdims=True)))

    def test_mean(self):
        for dtype in [numpy.float32]: # numpy.int32
            na = numpy.random.uniform(0., 10., (2,3,4)).astype(dtype)
            ga = backend.array(na)

            self.assertTrue(numpy.allclose(numpy.mean(na), backend.mean(ga)))
            self.assertTrue(numpy.allclose(numpy.mean(na, axis=0), backend.mean(ga, axis=0)))
            self.assertTrue(numpy.allclose(numpy.mean(na, axis=(0,2)), backend.mean(ga, axis=(0,2))))
            self.assertTrue(numpy.allclose(numpy.mean(na, axis=0, keepdims=True), backend.mean(ga, axis=0, keepdims=True)))
            nb = numpy.empty((1,3,4), dtype=dtype)
            gb = backend.empty((1,3,4), dtype=dtype)
            self.assertTrue(numpy.allclose(numpy.mean(na, axis=0, keepdims=True, out=nb), backend.mean(ga, axis=0, keepdims=True, out=gb)))

class LinearAlgebraTestCase(unittest.TestCase):
    def helper(self, ashape, bshape, cshape):
        na = numpy.random.uniform(0., 10., ashape)
        ga = backend.array(na)
        nb = numpy.random.uniform(0., 10., bshape)
        gb = backend.array(nb)
        nc = numpy.random.uniform(0., 10., cshape)
        gc = backend.array(nc)

        self.assertTrue(numpy.allclose(numpy.dot(na, nb), backend.dot(ga, gb)))
        self.assertTrue(numpy.allclose(numpy.dot(na, nb, out=nc), backend.dot(ga, gb, out=gc)))
        nc += numpy.dot(na, nb)
        backend.add_dot(ga, gb, gc)
        self.assertTrue(numpy.allclose(nc, gc))

        self.assertTrue(numpy.allclose(numpy.dot(na, nb.T), backend.dot(ga, gb.T)))
        self.assertTrue(numpy.allclose(numpy.dot(na.T, nb), backend.dot(ga.T, gb)))
        self.assertTrue(numpy.allclose(numpy.dot(na.T, nb.T), backend.dot(ga.T, gb.T)))

    def test_dotmm(self):
        self.helper((3,3), (3,3), (3,3))

    def test_dotmv(self):
        self.helper((3,3), (3,), (3,))

    def test_dotvm(self):
        self.helper((3,), (3,3), (3,))

    def test_dotvv(self):
        self.helper((3,), (3,), ())

    def test_outer(self):
        na = numpy.random.uniform(0., 10., (3,))
        ga = backend.array(na)
        nb = numpy.random.uniform(0., 10., (3,))
        gb = backend.array(nb)
        nc = numpy.random.uniform(0., 10., (3, 3,))
        gc = backend.array(nc)

        self.assertTrue(numpy.allclose(numpy.outer(na, nb), backend.outer(ga, gb)))
        self.assertTrue(numpy.allclose(numpy.outer(na, nb, out=nc), backend.outer(ga, gb, out=gc)))
        self.assertTrue(numpy.allclose(numpy.outer(na, nb, out=nc.T), backend.outer(ga, gb, out=gc.T)))
        nc += numpy.outer(na, nb)
        backend.add_outer(ga, gb, gc)
        self.assertTrue(numpy.allclose(nc, gc))

cases = [
    #CreationTestCase,
    #ShapeTestCase,
    #UnaryTestCase,
    #BinaryTestCase,
    #LogicalTestCase,
    ReductionTestCase,
    #LinearAlgebraTestCase,
]

alltests = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(case) for case in cases])
unittest.TextTestRunner(verbosity=2).run(alltests)
